#include <stdio.h>

void tmultiplicar (int t)
{ 
	int x;
	for (x=0;x<=10;x++)
	{
		printf("%d * %d = %d\n", t,x,(t*x));
		
	}
	
}

int main(){
	
	int numero;
	printf(" Introduzca un numero entero positivo de la tabla que desea imprimir: ");
	scanf("%d", &numero);
	tmultiplicar(numero);
	
	printf("\n\nElaborado por Jose Lorenzo, Cedula 5-705-319");
	return 0;
	
}
